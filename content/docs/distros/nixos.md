---
title: NixOS
---

# NixOS

General documentation about VR is provided on the [NixOS Wiki](https://wiki.nixos.org/wiki/VR).

Short overview:
- Monado is supported natively on NixOS using the [`services.monado`](https://search.nixos.org/options?channel=unstable&query=services.monado) since 24.05.
- SteamVR works like it does on other distros for the most part. Though there are some issues regarding the fhsenv-sandbox
  - [Asynchronous reprojection does not work](https://github.com/NixOS/nixpkgs/issues/217119) (without a kernel patch)
  - [setcap doesn't work](https://github.com/NixOS/nixpkgs/issues/42117#issuecomment-996731579) (but can be done manually)
- [Envision](../fossvr/envision) is currently being packaged in [this merge request](https://gitlab.com/gabmus/envision/-/merge_requests/11)
  - [Asynchronous reprojection does not work](https://github.com/NixOS/nixpkgs/issues/217119) (without a kernel patch)
